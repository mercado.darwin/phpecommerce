<?php 
require "../partials/template.php";

function get_title(){
echo "Profile";
}
function get_body_contents(){
require "../controllers/connection.php";
?>
<h1 class="text-center py-5">Profile Page</h1>
<div class="container">
<div class="col-lg-6 offset-lg-3">
<div class="card">
<img class="card-img-top rounded" id="profile" src="" alt="Card image cap">
<div class="card-body">
<h4 class="card-title text-center"><?php echo $_SESSION['user']['firstName']; echo " ";  echo $_SESSION['user']['lastName'];?></h4>
<h6 class="card-title text-center"><?= $_SESSION['user']['email'] ?></h6>

</div>
</div>
</div>
<div class="col-lg-10 offset-lg-1">
<h3>Addresses: </h3>
<ul>
<li class="list-group-item">
<?php
$user_id = $_SESSION['user']['id'];
$address_query = "SELECT * FROM addresses WHERE user_id = $user_id";
$indiv_address = mysqli_fetch_assoc(mysqli_query($conn, $address_query));

if(isset($indiv_address)){
echo $indiv_address['address1'].", ".$indiv_address['address2']. "<br/>" .$indiv_address['city']. ", " . $indiv_address['zipCode'];
}else {
echo "Add address..";
}

?>
</li>


</ul>
<form action="../controllers/add-address-process.php" method="POST">
<div class="form-group">
<label for="address1">Address 1</label>
<input type="text" name="address1" class="form-control">	
</div>
<div class="form-group">
<label for="address2">Address 2</label>
<input type="text" name="address2" class="form-control">	
</div>
<div class="form-group">
<label for="city">City</label>
<input type="text" name="city" class="form-control">	
</div>
<div class="form-group">
<label for="zipCode">Zip Code</label>
<input type="text" name="zipCode" class="form-control">	
</div>
<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
<button class="btn btn-secondary" type="submit"> Submit</button>

</form>

</div>

<div class="col-lg-10 offset-lg-1">
<h3>Contact: </h3>
<ul>
<li class="list-group-item">
<?php
$user_id = $_SESSION['user']['id'];
$contact_query = "SELECT * FROM contacts WHERE user_id = $user_id";
?>